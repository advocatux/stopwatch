/*****************************************************************************
 * Copyright: 2015 Michael Zanetti <michael_zanetti@gmx.net>                 *
 *                                                                           *
 * This prject is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This project is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                           *
 ****************************************************************************/

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0
import Stopwatch 1.0
import QtSystemInfo 5.0

MainView {
    id: root
    Component.onCompleted: Theme.name = "Ubuntu.Components.Themes.SuruDark"

    applicationName: "stopwatch.mzanetti"

    width: units.gu(40)
    height: units.gu(75)

    Settings {
        property alias startTime: stopwatch.startTime
        property alias running: stopwatch.running
        property alias oldDiff: stopwatch.oldDiff
    }

    History {
        id: history
    }

    ScreenSaver {
        screenSaverEnabled: !(Qt.application.active && (stopwatch.running || countdown.running))
    }

    Tabs {
        Tab {
            title: "Stopwatch"
            page: StopWatch {
                id: stopwatch
            }
        }

        Tab {
            title: "Countdown"
            page: Countdown {
                id: countdown
            }
        }
    }
}

