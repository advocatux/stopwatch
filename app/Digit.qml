/*****************************************************************************
 * Copyright: 2015 Michael Zanetti <michael_zanetti@gmx.net>                 *
 *                                                                           *
 * This prject is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This project is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                           *
 ****************************************************************************/

import QtQuick 2.4

Item {
    id: root
    implicitHeight: width * 2
    implicitWidth: number == "." || number == ":" ? height / 4 : height / 2

    property string number: "0"

    Repeater {
        model: ListModel {
            ListElement { digits: "02356789" }
            ListElement { digits: "045689" }
            ListElement { digits: "01234789" }
            ListElement { digits: "2345689" }
            ListElement { digits: "0268" }
            ListElement { digits: "013456789" }
            ListElement { digits: "0235689" }
            ListElement { digits: "." }
            ListElement { digits: ":" }
        }

        Image {
            anchors.centerIn: parent
            width: parent.width
            height: parent.height
            fillMode: Image.PreserveAspectFit
            horizontalAlignment: Image.AlignHCenter
            verticalAlignment: Image.AlignVCenter
            source: Qt.resolvedUrl((index + 1) + ".png")
            visible: model.digits.indexOf(root.number) != -1
        }
    }
}
